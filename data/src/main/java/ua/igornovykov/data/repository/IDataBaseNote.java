package ua.igornovykov.data.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.data.repository.local_database.database_helper.entity.NoteEntity;

public interface IDataBaseNote {
    Completable add(NoteEntity note);

    Completable remove(long noteId);

    Completable update(NoteEntity note);

    Single<List<NoteEntity>> getAllNotes();

    Single<NoteEntity> getNote(long noteId);
}
