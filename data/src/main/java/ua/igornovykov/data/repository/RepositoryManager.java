package ua.igornovykov.data.repository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.data.mapper.NoteMapper;
import ua.igornovykov.data.repository.local_database.DataBaseNoteRepository;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.domain.repository.INoteRepositoryBoundary;

public class RepositoryManager implements INoteRepositoryBoundary {
    private IDataBaseNote db;

    @Inject
    RepositoryManager(DataBaseNoteRepository db) {
        this.db = db;
    }

    @Override
    public Completable add(Note note) {
        return Single.just(note)
                .map(NoteMapper::transform)
                .flatMapCompletable(db::add);
    }

    @Override
    public Completable remove(long noteId) {
        return Single.just(noteId)
                .flatMapCompletable(db::remove);
    }

    @Override
    public Completable update(Note note) {
        return Single.just(note)
                .map(NoteMapper::transform)
                .flatMapCompletable(db::update);
    }

    @Override
    public Single<List<Note>> getAllNotes() {
        return db.getAllNotes()
                .map(NoteMapper::transformList);
    }

    @Override
    public Single<Note> getNote(long noteId) {
        return Single.just(noteId)
                .flatMap(db::getNote)
                .map(NoteMapper::transform);
    }
}
