package ua.igornovykov.data.repository.local_database.database_helper.migration;

import android.database.sqlite.SQLiteDatabase;

import javax.inject.Inject;

import ua.igornovykov.data.repository.local_database.database_helper.IMigration;

public class CreateNotesTableMigration implements IMigration {
    @Inject
    CreateNotesTableMigration() {
    }

    @Override
    public int getTargetVersion() {
        return 1;
    }

    @Override
    public void execute(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE notes (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, " +
                "description TEXT, " +
                "priority INTEGER)");
    }
}
