package ua.igornovykov.data.repository.local_database.database_helper;

import android.database.sqlite.SQLiteDatabase;

public interface IMigration {
    int getTargetVersion();

    void execute(SQLiteDatabase database);
}
