package ua.igornovykov.data.repository.local_database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.data.repository.IDataBaseNote;
import ua.igornovykov.data.repository.local_database.database_helper.DatabaseHelper;
import ua.igornovykov.data.repository.local_database.database_helper.entity.NoteEntity;

import static ua.igornovykov.data.repository.local_database.database_helper.entity.NoteEntity.TABLE_NOTES;
import static ua.igornovykov.data.repository.local_database.database_helper.entity.NoteEntity.COLUMN_ID;
import static ua.igornovykov.data.repository.local_database.database_helper.entity.NoteEntity.COLUMN_PRIORITY;
import static ua.igornovykov.data.repository.local_database.database_helper.entity.NoteEntity.COLUMN_TITLE;
import static ua.igornovykov.data.repository.local_database.database_helper.entity.NoteEntity.COLUMN_DESCRIPTION;

public class DataBaseNoteRepository implements IDataBaseNote {
    private DatabaseHelper databaseHelper;

    @Inject
    DataBaseNoteRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    @Override
    public Completable add(NoteEntity note) {
        return Completable.fromAction(() -> addNewNote(note));
    }

    @Override
    public Completable update(NoteEntity note) {
        return Completable.fromAction(() -> updateNote(note));
    }

    @Override
    public Single<List<NoteEntity>> getAllNotes() {
        return Single.fromCallable(this::getListNotes);
    }

    @Override
    public Single<NoteEntity> getNote(long noteId) {
        return Single.fromCallable(() -> getNoteById(noteId));
    }

    public Completable remove(long noteId) {
        return Completable.fromAction(() -> removeNote(noteId));
    }

    private void addNewNote(NoteEntity noteEntity) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.insert(TABLE_NOTES, null, noteEntity.getContentValues());
        db.close();
    }

    private void updateNote(NoteEntity noteEntity) {
        String whereClause = COLUMN_ID + " = ?";
        String[] whereArgs = new String[]{String.valueOf(noteEntity.getId())};

        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.update(TABLE_NOTES, noteEntity.getContentValues(), whereClause, whereArgs);
        db.close();
    }

    private List<NoteEntity> getListNotes() {
        String query = "SELECT * FROM " + TABLE_NOTES;

        List<NoteEntity> notes = new LinkedList<>();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        try (Cursor cursor = db.rawQuery(query, null)) {
            if (cursor.moveToFirst()) {
                do {
                    long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                    String title = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE));
                    String description = cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION));
                    int priority = cursor.getInt(cursor.getColumnIndex(COLUMN_PRIORITY));

                    notes.add(new NoteEntity(id, title, description, priority));
                } while (cursor.moveToNext());
            }
        }
        db.close();
        return notes;
    }

    private NoteEntity getNoteById(long noteId) {
        NoteEntity noteEntity = new NoteEntity();
        String query = "SELECT * FROM " + TABLE_NOTES + " WHERE " + COLUMN_ID + " =?";
        String[] args = new String[]{String.valueOf(noteId)};

        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        try (Cursor cursor = db.rawQuery(query, args)) {
            if (cursor.moveToFirst()) {
                noteEntity = new NoteEntity(
                        cursor.getLong(cursor.getColumnIndex(COLUMN_ID)),
                        cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)),
                        cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                        cursor.getInt(cursor.getColumnIndex(COLUMN_PRIORITY)));
            }
        }
        db.close();
        return noteEntity;
    }


    private void removeNote(long noteId) {
        String whereClause = COLUMN_ID + " = ?";
        String[] whereArgs = new String[]{String.valueOf(noteId)};

        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.delete(TABLE_NOTES, whereClause, whereArgs);
        db.close();
    }
}
