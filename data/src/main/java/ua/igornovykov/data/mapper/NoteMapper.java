package ua.igornovykov.data.mapper;

import java.util.ArrayList;
import java.util.List;

import ua.igornovykov.data.repository.local_database.database_helper.entity.NoteEntity;
import ua.igornovykov.domain.entity.Note;

public class NoteMapper {
    public static NoteEntity transform(Note note) {
        return new NoteEntity(
                note.getId(),
                note.getNoteTitle(),
                note.getNoteDescription(),
                note.getPriorityToInt());
    }

    public static Note transform(NoteEntity noteEntity) {
        return new Note(
                noteEntity.getId(),
                noteEntity.getTitle(),
                noteEntity.getDescription(),
                noteEntity.getPriority());
    }

    public static List<Note> transformList(List<NoteEntity> notes) {
        List<Note> list = new ArrayList<>(notes.size());
        for (NoteEntity noteEntity : notes) {
            list.add(transform(noteEntity));
        }
        return list;
    }
}
