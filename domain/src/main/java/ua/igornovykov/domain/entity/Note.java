package ua.igornovykov.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class Note implements Comparable<Note>, Parcelable {
    private long id;
    private String noteTitle;
    private String noteDescription;
    private NotePriority priority;

    public Note(String title, String description, NotePriority priority) {
        this(0, title, description, priority);
    }
    public Note(long id, String title, String description, String priority) {
        this(id, title, description, NotePriority.valueOf(priority));
    }

    public Note(long id, String title, String description, int priority) {
        this(id, title, description, NotePriority.values()[priority]);
    }

    public Note(long id ,String noteTitle, String noteDescription, NotePriority priority) {
        this.id = id;
        this.noteTitle = noteTitle;
        this.noteDescription = noteDescription;
        this.priority = priority;
    }

    protected Note(Parcel in) {
        id = in.readLong();
        noteTitle = in.readString();
        noteDescription = in.readString();
        priority = NotePriority.valueOf(in.readString());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(noteTitle);
        dest.writeString(noteDescription);
        dest.writeString(priority.name());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    @Override
    public int compareTo(@NonNull Note note) {
        int result;
        result = Integer.compare(priority.ordinal(), note.getPriority().ordinal());
        if (result != 0) {
            return result;
        }
        result = Long.compare(id, note.getId());
        if (result != 0) {
            return result;
        }
        return noteDescription.compareTo(note.noteDescription);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof Note)) return false;
        Note note = (Note) o;
        return id == note.id &&
                Objects.equals(noteTitle, note.noteTitle) &&
                Objects.equals(noteDescription, note.noteDescription) &&
                priority == note.priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, noteTitle, noteDescription, priority);
    }

    public long getId() {
        return id;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public String getNoteDescription() {
        return noteDescription;
    }

    public NotePriority getPriority() {
        return priority;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public int getPriorityToInt() {
        return priority.ordinal();
    }

    public enum  NotePriority {
        HIGH, MIDDLE, LOW
    }
}
