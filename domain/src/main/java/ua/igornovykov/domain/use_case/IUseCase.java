package ua.igornovykov.domain.use_case;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.domain.entity.Note;

public interface IUseCase {
    Completable add(Note note);

    Completable remove(long noteId);

    Completable update(Note note);

    Single<Note> getNote(long noteId);

    Single<List<Note>> getNoteList();
}
