package ua.igornovykov.domain.use_case.interactor;

import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.domain.repository.INoteRepositoryBoundary;
import ua.igornovykov.domain.use_case.IUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class NoteListInteractor implements IUseCase {
    private INoteRepositoryBoundary boundary;
    @Inject
    public NoteListInteractor(INoteRepositoryBoundary boundary) {
        this.boundary = boundary;
    }

    @Override
    public Completable add(Note note) {
        return boundary.add(note);
    }

    @Override
    public Completable remove(long noteId) {
        return boundary.remove(noteId);
    }

    @Override
    public Completable update(Note note) {
        return boundary.update(note);
    }

    @Override
    public Single<Note> getNote(long noteId) {
        return boundary.getNote(noteId);
    }

    @Override
    public Single<List<Note>> getNoteList() {
        return boundary.getAllNotes();
    }
}
