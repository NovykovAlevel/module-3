package ua.igornovykov.domain.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.domain.entity.Note;

public interface INoteRepositoryBoundary {
    Completable add(Note note);

    Completable remove(long noteId);

    Completable update(Note note);

    Single<List<Note>> getAllNotes();

    Single<Note> getNote(long noteId);
}
