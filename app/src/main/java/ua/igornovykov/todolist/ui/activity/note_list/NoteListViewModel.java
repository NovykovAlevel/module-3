package ua.igornovykov.todolist.ui.activity.note_list;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.domain.use_case.IUseCase;
import ua.igornovykov.todolist.ui.activity.Event;

public class NoteListViewModel extends ViewModel {
    private IUseCase useCase;
    private CompositeDisposable composite = new CompositeDisposable();

    private MutableLiveData<List<Note>> allNotes = new MutableLiveData<>();
//    private MutableLiveData<Boolean> isShowProgress = new MutableLiveData<>();
    private MutableLiveData<Event<Long>> startEditNoteActivity = new MutableLiveData<>();

    @Inject
    public NoteListViewModel(IUseCase useCase) {
        this.useCase = useCase;
        getAllNotes();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        composite.dispose();
    }

    LiveData<List<Note>> observeAllNotes() {
        return allNotes;
    }

//    LiveData<Boolean> observeShowProgress() {
//        return isShowProgress;
//    }

    LiveData<Event<Long>> observeStartEditNoteActivity() {
        return startEditNoteActivity;
    }

    void add(Note note) {
        composite.add(useCase.add(note)
                .subscribeOn(Schedulers.io())
                .subscribe(this::getAllNotes));
    }

    void remove(Note note) {
        composite.add(useCase.remove(note.getId())
                .subscribeOn(Schedulers.io())
                .subscribe(this::getAllNotes));
    }

    void getAllNotes() {
        composite.add(useCase.getNoteList()
//                .doOnSubscribe(disposable -> isShowProgress.postValue(true))
//                .doOnSuccess(list -> isShowProgress.postValue(false))
                .subscribeOn(Schedulers.io())
                .subscribe(allNotes::postValue));
    }

    void startEditNoteActivity(long noteId) {
        startEditNoteActivity.postValue(new Event<>(noteId));
    }
}
