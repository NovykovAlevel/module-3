package ua.igornovykov.todolist.ui.activity.note_list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import dagger.android.support.DaggerAppCompatActivity;

import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.todolist.R;
import ua.igornovykov.todolist.ui.activity.Event;
import ua.igornovykov.todolist.ui.fragment.DialogNote;
import ua.igornovykov.todolist.ui.activity.note_editor.NoteEditActivity;
import ua.igornovykov.todolist.ui.adapter.NoteAdapter;
import ua.igornovykov.todolist.ui.adapter.NoteAdapterClickListener;

public class MainActivity extends DaggerAppCompatActivity implements DialogNote.OnSaveClickListener,NoteAdapterClickListener,
         NoteAdapter.OnRemoveNoteListener, View.OnClickListener {

    private static final int REQUEST_CODE_KEY = 101;

    private View containerList;
    private RecyclerView noteRecyclerView;

    @Inject
    NoteAdapter adapter;

    @Inject
    NoteListViewModelFactory vmFactory;

    private NoteListViewModel vm;

    public static Intent getIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        containerList = findViewById(R.id.container_list);
        noteRecyclerView = findViewById(R.id.noteRecyclerView);

        FloatingActionButton addNoteFAB = findViewById(R.id.addNoteFAB);
        addNoteFAB.setOnClickListener(this);

        initAdapter();
        initNoteRecyclerView();
        initViewModel();

        observerNewDataList();
        observerStartNoteEditActivity();
    }

    private void initViewModel() {
        vm = ViewModelProviders
                .of(this, vmFactory)
                .get(NoteListViewModel.class);
    }

    private void initNoteRecyclerView() {
        noteRecyclerView.setAdapter(adapter);
        noteRecyclerView.setLayoutManager(getManager());
    }

    @NonNull
    private RecyclerView.LayoutManager getManager() {
        return new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
    }

    private void initAdapter() {
        adapter.setNoteAdapterClickListener(this);
        adapter.setRemoveListener(this);
    }

    @Override
    public void onSave(Note note) {
        vm.add(note);
    }

    @Override
    public void onErrorSaveClick(String message) {

    }

    @Override
    public void onClicked(long noteId) {
        vm.startEditNoteActivity(noteId);
    }

    @Override
    public void onRemoveNote(Note note) {
        vm.remove(note);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addNoteFAB:
                DialogNote dialogNoteFragment = DialogNote.newInstance();
                dialogNoteFragment.show(getSupportFragmentManager(), "DialogNote");
        }
    }

    private void observerNewDataList() {
        vm.observeAllNotes().observe(this, adapter::setData);
    }

    private void observerStartNoteEditActivity() {
        vm.observeStartEditNoteActivity().observe(this, this::goToEditor);
    }

    public void goToEditor(Event<Long> event) {
        if (!event.isHandled()) {
            event.setHandled(true);
            Intent intent = NoteEditActivity.getIntent(this, event.getData());
            startActivityForResult(intent, REQUEST_CODE_KEY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode){
                case REQUEST_CODE_KEY:
                    vm.getAllNotes();
                    break;
            }
        }
    }

}

