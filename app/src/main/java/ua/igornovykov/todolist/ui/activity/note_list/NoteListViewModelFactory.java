package ua.igornovykov.todolist.ui.activity.note_list;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class NoteListViewModelFactory implements ViewModelProvider.Factory {
    private NoteListViewModel vm;

    @Inject
    public NoteListViewModelFactory(NoteListViewModel vm) {
        this.vm = vm;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) vm;
    }
}
