package ua.igornovykov.todolist.ui.activity.note_editor;

import android.util.Log;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.domain.use_case.IUseCase;
import ua.igornovykov.todolist.ui.activity.Event;

public class NoteEditorViewModel extends ViewModel {
    private IUseCase useCase;
    private CompositeDisposable composite = new CompositeDisposable();

    private MutableLiveData<Integer> showSelectedPriority = new MutableLiveData<>();
    private MutableLiveData<Event<Note>> editableNote = new MutableLiveData<>();
    private MutableLiveData<Boolean> isSuccessfulUpdate = new MutableLiveData<>();

    @Inject
    public NoteEditorViewModel(IUseCase useCase) {
        this.useCase = useCase;
    }

    void getNote(long noteId) {
        if (editableNote.getValue() == null) {
            composite.add(useCase.getNote(noteId)
                    .subscribeOn(Schedulers.io())
                    .subscribe(note -> editableNote.postValue(new Event<>(note)), this::logger));
        }
    }
    void showSelectedPriority(int priority) {
        showSelectedPriority.postValue(priority);
    }

    void update(Note note) {
        composite.add(useCase.update(note)
                .subscribeOn(Schedulers.io())
                .subscribe(() -> isSuccessfulUpdate.postValue(true), this::logger));
    }

    LiveData<Event<Note>> observeEditableNote() {
        return editableNote;
    }

    LiveData<Integer> observeSelectedPriority() {
        return showSelectedPriority;
    }

    LiveData<Boolean> observeUpdatedNoteId() {
        return isSuccessfulUpdate;
    }

    private void logger(Throwable throwable) {
        Log.e("ALevel", "logger: " + throwable.getMessage());
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        composite.dispose();
    }
}
