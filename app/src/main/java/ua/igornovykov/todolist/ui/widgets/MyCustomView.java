package ua.igornovykov.todolist.ui.widgets;

import android.content.Context;
import android.content.res.ColorStateList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatImageView;
import ua.igornovykov.todolist.R;

public class MyCustomView extends LinearLayout {
    private AppCompatImageView priorityIcon;

    public MyCustomView(@NonNull Context context) {
       this(context,null);
    }

    public MyCustomView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyCustomView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }
    private void initView(Context context){
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.widget_custom_view, this,true);
        priorityIcon = view.findViewById(R.id.priorityIcon);
    }
    public void setImageResource(int resId){
        priorityIcon.setImageResource(resId);
    }
    public void setBackgroundTintList(ColorStateList tint) {
        priorityIcon.setBackgroundTintList(tint);
    }
}
