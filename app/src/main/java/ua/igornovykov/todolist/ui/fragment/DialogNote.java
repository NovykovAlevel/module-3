package ua.igornovykov.todolist.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.todolist.R;

public class DialogNote extends DialogFragment implements View.OnClickListener {

    private AppCompatEditText noteTitleEditText;
    private AppCompatEditText noteDescriptionEditText;
    private AppCompatSpinner spinnerPriority;
    private OnSaveClickListener listener;


    private void initView(View view) {
        noteTitleEditText = view.findViewById(R.id.noteTitleEditText);
        noteDescriptionEditText = view.findViewById(R.id.noteDescriptionEditText);
        spinnerPriority = view.findViewById(R.id.spinnerPriority);
        AppCompatButton cancelButton = view.findViewById(R.id.cancelButton);
        AppCompatButton saveButton = view.findViewById(R.id.saveButton);

        cancelButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnSaveClickListener) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnSaveClickListener) {
            listener = (OnSaveClickListener) activity;
        } else {
            throw new IllegalStateException("Implement OnSaveClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public static DialogNote newInstance() {
        return new DialogNote();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_create_note, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelButton:
                dismiss();
                break;
            case R.id.saveButton:
                if (checkInputData()) {
                    listener.onSave(createNewNote());
                    dismiss();
                }
                break;
        }
    }

    private boolean checkInputData() {
        if (listener == null) {
            return false;
        }
        if (TextUtils.isEmpty(noteTitleEditText.getText())) {
            listener.onErrorSaveClick("Forget title");
            return false;
        }
        if (TextUtils.isEmpty(noteDescriptionEditText.getText())) {
            listener.onErrorSaveClick("Forget description");
            return false;
        }
        return true;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    private Note createNewNote() {
        return new Note(noteTitleEditText.getText().toString(),
                noteDescriptionEditText.getText().toString(),
                Note.NotePriority.valueOf(spinnerPriority.getSelectedItem().toString()));
    }

    public interface OnSaveClickListener {
        void onSave(Note note);
        void onErrorSaveClick(String message);
    }
}
