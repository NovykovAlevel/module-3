package ua.igornovykov.todolist.ui.activity.note_editor;


import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.AndroidInjection;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.todolist.R;
import ua.igornovykov.todolist.ui.activity.Event;
import ua.igornovykov.todolist.ui.activity.note_list.MainActivity;

public class NoteEditActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String INTENT_KEY_NOTE = "INTENT_KEY_NOTE";
    private Long noteId;

    private NoteEditorViewModel vm;

    private AppCompatEditText noteTitleEditText;
    private AppCompatEditText noteDescriptionEditText;
    private AppCompatTextView notePriorityTextView;

    @Inject
    NoteEditModelFactory viewModelFactory;

    public static Intent getIntent(Context context, long noteId) {
        Intent intent = new Intent(context, NoteEditActivity.class);
        intent.putExtra(INTENT_KEY_NOTE, noteId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_edit);

        initViews();
        initViewModel();

        getDataFromIntent();

        observerShowPriority();
        observerEditableNote();
        observerUpdatedNoteId();

//        getPriorityColor();
    }

    private void initViews() {
        noteTitleEditText = findViewById(R.id.noteTitleEditText);
        noteDescriptionEditText = findViewById(R.id.noteDescriptionEditText);
        notePriorityTextView = findViewById(R.id.notePriorityTextView);

        findViewById(R.id.lowPriorityButton).setOnClickListener(this);
        findViewById(R.id.middlePriorityButton).setOnClickListener(this);
        findViewById(R.id.highPriorityButton).setOnClickListener(this);
        findViewById(R.id.saveNoteFAB).setOnClickListener(this);
    }
    private void initViewModel() {
        vm = ViewModelProviders
                .of(this, viewModelFactory)
                .get(NoteEditorViewModel.class);
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        noteId = intent.getLongExtra(INTENT_KEY_NOTE, -1);
        if (noteId > -1){
            vm.getNote(noteId);
        }
    }

    private void observerShowPriority() {
        vm.observeSelectedPriority().observe(this, notePriorityTextView::setText);
    }

    private void observerEditableNote() {
        vm.observeEditableNote().observe(this, this::filingFields);
    }

    private void observerUpdatedNoteId() {
        vm.observeUpdatedNoteId().observe(this, this::editNote);
    }

    private void editNote(Boolean isSuccess) {
        Log.d("nik", "editNote: " + isSuccess);

        if (isSuccess) {

            Log.d("nik", "editNote: " );
            Intent intent = MainActivity.getIntent(this);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void filingFields(Event<Note> event) {
        if (!event.isHandled()) {
            event.setHandled(true);
            noteTitleEditText.setText(event.getData().getNoteTitle());
            noteTitleEditText.setSelection(noteTitleEditText.length());
            noteDescriptionEditText.setText(event.getData().getNoteDescription());
            notePriorityTextView.setText(event.getData().getPriority().name());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lowPriorityButton:
                vm.showSelectedPriority(R.string.low);
                break;
            case R.id.middlePriorityButton:
                vm.showSelectedPriority(R.string.middle);
                break;
            case R.id.highPriorityButton:
                vm.showSelectedPriority(R.string.high);
                break;
            case R.id.saveNoteFAB:
                Log.d("nik", "onClick: ");
                vm.update(createNewNote());
                break;

        }
    }

    private Note createNewNote() {
        return new Note(
                noteId,
                noteTitleEditText.getText().toString(),
                noteDescriptionEditText.getText().toString(),
                notePriorityTextView.getText().toString());
    }

//    private void getPriorityColor() {
//        if (notePriorityTextView.getText() == Note.NotePriority.LOW.toString()) {
//            notePriorityTextView.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
//        }
//        if (notePriorityTextView.getText() == Note.NotePriority.MIDDLE.toString()) {
//            notePriorityTextView.setBackgroundTintList(ColorStateList.valueOf(Color.YELLOW));
//        }
//        if (notePriorityTextView.getText() == Note.NotePriority.HIGH.toString()) {
//            notePriorityTextView.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
//        }
//    }

//    private void saveEditedNote() {
//        note.setNoteTitle(noteTitleEditText.getText().toString());
//        note.setNoteDescription(noteDescriptionEditText.getText().toString());
//        note.setPriority(Note.NotePriority.valueOf(notePriorityTextView.getText().toString()));
//    }
}
