package ua.igornovykov.todolist.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import ua.igornovykov.todolist.ui.activity.note_list.MainActivity;
import ua.igornovykov.todolist.ui.activity.note_editor.NoteEditActivity;

@Module(includes = AndroidSupportInjectionModule.class)
interface DaggerAndroidModule {
    @ContributesAndroidInjector
    MainActivity mainActivity();

    @ContributesAndroidInjector()
    NoteEditActivity noteEditActivity();

}
