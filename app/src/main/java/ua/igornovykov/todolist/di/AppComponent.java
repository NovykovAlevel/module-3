package ua.igornovykov.todolist.di;

import android.content.Context;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import ua.igornovykov.todolist.Application;
import ua.igornovykov.todolist.di.scope.ApplicationScope;

@ApplicationScope
@Component(modules = {DaggerAndroidModule.class, DataModule.class, MigrationDatabaseModule.class})
public interface AppComponent extends AndroidInjector<Application> {
    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder context(Context context);

        AndroidInjector<? extends DaggerApplication> build();
    }
}
