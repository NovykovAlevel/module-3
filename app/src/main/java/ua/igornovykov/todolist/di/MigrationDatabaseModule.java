package ua.igornovykov.todolist.di;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;

import ua.igornovykov.data.repository.local_database.database_helper.IMigration;
import ua.igornovykov.data.repository.local_database.database_helper.migration.CreateNotesTableMigration;

@Module
public interface MigrationDatabaseModule {
    @IntoSet
    @Binds
    IMigration provideCreateNotesMigration(CreateNotesTableMigration createNotesTableMigration);
}
