package ua.igornovykov.todolist.di;

import dagger.Binds;
import dagger.Module;
import ua.igornovykov.data.repository.IDataBaseNote;
import ua.igornovykov.data.repository.RepositoryManager;
import ua.igornovykov.data.repository.local_database.DataBaseNoteRepository;
import ua.igornovykov.domain.repository.INoteRepositoryBoundary;
import ua.igornovykov.domain.use_case.IUseCase;
import ua.igornovykov.domain.use_case.interactor.NoteListInteractor;
import ua.igornovykov.todolist.di.scope.ApplicationScope;
@Module
public interface DataModule {
    @ApplicationScope
    @Binds
    INoteRepositoryBoundary provideRepositoryManager(RepositoryManager repositoryManager);

    @ApplicationScope
    @Binds
    IDataBaseNote provideDBNoteRepository(DataBaseNoteRepository dbNoteRepository);

    @Binds
    IUseCase provideNoteListInteractor(NoteListInteractor noteListInteractor);
}
